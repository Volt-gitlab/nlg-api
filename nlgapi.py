# -*- coding: utf-8 -*-
""" Construction d'une api offrant les services d'un modele NLG entrainé avec Ngram """
# 1. Preparation des donnnes d'entrainement


#from nltk.translate.bleu_score import sentence_bleu
from NgramModel import NgramModel
import csv # importation de la librairie permettant de creer le fichier csv
import time

from flask import Flask, request
from flask_restful import Resource, Api
import json
import random
import numpy as np
from flasgger import Swagger
from flasgger.utils import swag_from
from flasgger import LazyString, LazyJSONEncoder

#from routes import request_api

app = Flask(__name__, template_folder='../templates', static_folder='../static') # creation de l'aplication Flask
api = Api(app) # Creation de l'api

# Add documentation of API
app.config["SWAGGER"] = {"title": "NLG - API", "uiversion": 1}

swagger_config = {
    "headers": [],
    "specs": [
        {
            "endpoint": "apispec_1",
            "route": "/apispec_1.json",
            "rule_filter": lambda rule: True,  # all in
            "model_filter": lambda tag: True,  # all in
        }
    ],
    "static_url_path": "/flasgger_static",
    # "static_folder": "static",  # must be set by user
    "swagger_ui": True,
    "specs_route": "/swagger/",
}

template = dict(
    swaggerUiPrefix=LazyString(lambda: request.environ.get("HTTP_X_SCRIPT_NAME", ""))
)

app.json_encoder = LazyJSONEncoder
swagger = Swagger(app, config=swagger_config, template=template)
# end of adding

corpus = []
# recuperation des references dans le fichier csv
with open('references.csv', 'r', encoding='UTF8') as file :
    
    reader = csv.reader(file)

    # i get data
    for i in reader:
      ref = ''.join(i)
      corpus.append(ref.split(' '))


# 2 Entrainement des differents modeles

first_modele_5 = NgramModel(corpus, 5)
second_modele_7 = NgramModel(corpus, 7)
third_modele_9 = NgramModel(corpus, 9)
fourth_modele_11 = NgramModel(corpus, 11)

# 3 generation par chacun des modeles et calcul des scores

def modele1(sentence):

  modele1 = first_modele_5 # copie du modele deja entrainé
  result = modele1.generate_sentence(sentence) # generation de texte
  if type(result) == str: # au cas ou il devait me retourner un message d'erreur
    return []
  return result


def modele2(sentence):

  modele2 = second_modele_7 # copie du modele deja entrainé
  result = modele2.generate_sentence(sentence) # generation de texte
  if type(result) == str: # au cas ou il devait me retourner un message d'erreur
    return []
  return result


def modele3(sentence):

  modele3 = third_modele_9 # copie du modele deja entrainé
  result = modele3.generate_sentence(sentence) # generation de texte
  if type(result) == str: # au cas ou il devait me retourner un message d'erreur
    return []
  return result  


def modele4(sentence):

  modele4 = fourth_modele_11 # copie du modele deja entrainé
  result = modele4.generate_sentence(sentence) # generation de texte
  if type(result) == str: # au cas ou il devait me retourner un message d'erreur
    return []
  return result


# 3. Creation de l'endpoint qui genere la phrase finale

all_candidats = []
class GetSentence(Resource):

    @swag_from("static/swagger_config.yml")
    def post(self):
        start = time.time()
        score_max = 0
        final_sentence = " I don't understand what you mean ..."
        list_final_sentence = []

        sentence = request.form['data']
        if sentence == "" or sentence ==" ":
            return json.dumps({"message" : " Enter sentence with at least one character"}), 400
        else :
            all_candidats = modele1(sentence) + modele2(sentence) + modele3(sentence) + modele4(sentence)
            for candidat in all_candidats:
              candidat_list = fourth_modele_11.return_token(candidat) # je transforme en liste de tokens pour passer a la fonction d'evaluation
              score = fourth_modele_11.evaluate(references = corpus, candidat = candidat_list, weights=(0.05, 0.15, 0.3, 0.5)) # les poids sont organisés de la sorte pour privilegier les modeles dont les generations sont egales en taille de 4
              if score >= score_max and score != 1:
                score_max = score
                final_sentence = candidat
              elif score == 1:
                list_final_sentence.append(candidat) # Pour garder ceux dont le score est 1, et faire un random parmi

            end = time.time()

            print(end - start, "secondes")

            if len(all_candidats) <= 1 :
              print("woow")
              return json.dumps({"sentence" : final_sentence}), 400 

            elif len(list_final_sentence) == 0 :
              return json.dumps({"sentence" : final_sentence})

            else:
              return json.dumps({"sentence" : random.choice(list_final_sentence)})

             
                
api.add_resource(GetSentence, '/generate/')

if __name__ == '__main__':
    app.run(debug=True)