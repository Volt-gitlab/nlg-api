# -*- coding: utf-8 -*-

from nltk.translate.bleu_score import sentence_bleu
import numpy as np
import math
import random
import itertools
import spacy # importation de la librairie
#!python -m spacy download fr_core_news_sm # pipeline pour le francais

class NgramModel():

    def __init__(self, dataset, ngramsize = 10):

      self.word_to_index_unigram = {}
      self.index_to_word_unigram = []
      self.word_to_index_ngram = {}
      self.index_to_word_ngram = []
      self.matrix = np.ones((10, 10), dtype = float)
      self.ngram_size = 10
      self.train(dataset, ngramsize) # entrainement à l'instanciation

    # Fonction de tokenisation
    def return_token(self, sentence):
      #print("tokenise ...")  
      nlp = spacy.load("fr_core_news_sm")
      # Tokeniser la phrase
      doc = nlp(sentence)
      # Retourner le texte de chaque token
      return [X.text for X in doc if X.text != ' ']


    # La fonction qui me permet de creer la liste des sequences de mots et le dictionaire  seequence de mot : index
    def create_word_index_ngram(self, corpus, ngram_size = 10): # La fonction prend dorenavant un parametre precisant la taille des grams (initialisee à 3)
      print("create word to index ngram ... ")
      ngram_size = self.ngram_size
      first_chaine = "" # Comme il faut creer une chaine de START de taille ngramsize, je l'initialise
      for _ in range(ngram_size):
        first_chaine = first_chaine + "START "
      first_chaine = first_chaine[:-1] # j'enleve le dernier caractere qui est l'espace, cest la chaine START START START...  qui a l'id 0
      word_to_index = {
      first_chaine:0,
      "END":1 # et END a l'id 1
      }
      index_to_word = [first_chaine, 'END'] # J'initialise la liste des mots

      for speech in corpus: # je parcours chaque phrase de mon corpus
          for i in range(len(speech)): # pour chaque mot de la phrse, non pas mot mais token
              word = "" # variable qui va contenir la sequence des mots, la sequence a la taille de ngramsize
              if i < ngram_size -1 : # Dans cette condition je constitue la sequence, et ici specifiquement elle commence par START
                  word = 'START ' * (ngram_size - i - 1)
                  for k in range(i+1): # Je concatene donc le debut des start la avec les tokens suivants
                      word = word + speech[k] + ' '
              else: # Et ici je constitue les sequences qui ne commencent pas par START, genre ce sont les mots du milieu de la phrase, pas du debut
                  for k in range(i-(ngram_size-1), i+1):
                      word = word + speech[k] + ' '

              word = word[:-1] # j'enleve l'espace de la fin
              if word not in index_to_word: # si la sequence n'est pas encore dans la liste je vais l'ajouter, a la derniere ligne
                  word_to_index[word] = len(index_to_word) # et je l'ajoute aussi dans le dictionnaire avec son id est son emplacement  dans la liste des sequences
                  index_to_word.append(word)
              
      return word_to_index, index_to_word # Je retourne le dictionnaire et la liste...  


    # cette fonction me permet de definir la matrice contenant sequence et mot suivant, les sequences etant de taille ngram_size
    def create_ngram_matrix(self, word_to_index, index_to_word, word_to_index_unigram, corpus, ngram_size = 10):
      print(" create ngram matrix ...")
      ngram_size = self.ngram_size
      V = len(index_to_word) 
      V_unigram = len(word_to_index_unigram)
      matrix = np.zeros((V, V_unigram), dtype='uint8') # Je definis ma matrice, qui est de taille (nombre de sequence de mots * nombre de mots unigrams)

      first_chaine = "" # Je fabrique la meme premiere chaine la que je vais ajouter a la colonne 0 de la matrice
      for _ in range(ngram_size):
        first_chaine = first_chaine + "START "
      first_chaine = first_chaine[:-1]

      for sentence in corpus:
          for i in range(len(sentence)): # Je parcours donc chaque token de la liste des tokens de la phrase en question
              if i < ngram_size-1: # Dans cette condition je verifie que le token actuel est parmi les premiers tokens de la phrase, cest a dire pour les cas ou il n'a pas tous les token precedent s pour avoir la sequence complete à ettre dans la matrice
                  taille_start = ""
                  if i == 0: # a chaque fois je vais mettre la chaine START START ... dans la premiere colonne de la matrice
                      matrix[word_to_index[first_chaine], word_to_index_unigram[sentence[i]]] += 1 # et mettre cette case à 1 pour marquer le debut d'une nouvelle phrase
                  if len(sentence) > i+1: # si la phrse est asseez longue, je mets la suite des sequences (ici ce sont les sequences qui commencent par START )
                      # je fabrique la chaine qui commence par START à faire correspondre en ligne dans la matrice puis je concatene avec les premiers mots et je fais la correspondance dans la matrice cad chercher son successeur
                      taille_start = "START " * (ngram_size-i-1) 
                      for j in range(i+1):
                          taille_start = taille_start + sentence[j] + " "
                      matrix[word_to_index[taille_start[:-1]], word_to_index_unigram[sentence[i+1]]] += 1  # j'incremente donc la case correspondant a la sequence et au successeur

              elif i >= (len(sentence)-1): # Il s'agit ici du cas ou je suis au dernier caractere de la phrase
                  # je fabrique la chaine dont je cherche a mettre le successeur à END et la taille de la chaine est en fonction de la taille du ngram
                  taille_chaine = ""
                  for l in range(i-(ngram_size-1), i+1):
                      taille_chaine = taille_chaine + sentence[l] + " "
                  matrix[word_to_index[taille_chaine[:-1]], 1] += 1 # je met la colonne de END à 1
              else: # Il s'agit donc du cas où je suis au milieur de la phrase, quand je tombe sur un mot (ou token), je prends simplement ses precedents, autant qu'il en faut pour faire correspondre a la taille de ngramsize
                  taille_chaine = "" 
                  for l in range(i-(ngram_size-1), i+1): # Je concatene ce mot et ses precedents
                      taille_chaine = taille_chaine + sentence[l] + " "
                  matrix[word_to_index[taille_chaine[:-1]], word_to_index_unigram[sentence[i+1]]] += 1 # Puis j'increment la case correspondant e avec le sucesseur en colonne 
      
      return matrix    # Je retourne ensuite la matrice, toute faite
      
    # Cette fonction permet de constituer la liste des mots (sequence de un seul mot), et le dictionnaire correspondant mot : index
    def create_word_index_unigram(self, corpus):
     
      word_to_index = {
      "START":0,
      "END":1
      }
      
      index_to_word = ['START', 'END']

      for speech in corpus:
          for word in speech: # je parcours chaque phrase  je parcours ses tokens
              if word not in index_to_word: # et je n''ajoute un token que s'il n'est pas deja dans la liste des tokens 
                  word_to_index[word] = len(index_to_word) # et dans le dictionnaire, la valeur pour la cle qui est le mot, est simplement a position de ce mot dans la liste des tokens
                  #print(len(index_to_word))
                  index_to_word.append(word) # ensuite j'ajoute ce token
                  
      return word_to_index, index_to_word # et je retourne le dictionnaire et la liste
    

    # Cette fonction prend en entree l'index d'une sequenc de mot pour retourner l'index de son successeur
    def get_next_word(self, index_previous_ngram, matrix, show_nb_possibilities=False): # il prend l'index du mot et retourne l'index du mot suivant
      
      '''
      index_previous_ngram : int, represent the word index in the word_to_index matrix
      matrix : successor matrix, lines represent the n-gram, columns represent the successor

      1. Check how many successors are possible
      2. Select a random word from the possible words, to avoid repetition
      '''
      
      if (matrix[index_previous_ngram]>0).sum() < 2:
          nb_possible = 1
      elif (matrix[index_previous_ngram]>0).sum() > 9:
          nb_possible = 10
      else:
          nb_possible = (matrix[index_previous_ngram]>0).sum()

      if show_nb_possibilities==True:
          print('NUMBER POSSIBLE ', nb_possible)

      top_indexes = matrix[index_previous_ngram].argsort()[-nb_possible:] # constitue la liste des 10 mots suivants les plus succeptibles (avec le plus d'occurence) , 10 etant le nombre de possibiltes, il varie en trre 1 et 10
      random_index = math.floor(random.random()*nb_possible) # juste pour choisir aleatoirement  l'index du prochin mot parmi toutes les possibilites

      index_next_word = top_indexes[random_index] # on choisi un index aleatoirement dans cet ensemble des 10 possibilites
      
      if matrix[index_previous_ngram][index_next_word] == 0: # Si aucun mot n'est successeur, je renvoie le END
        index_next_word = 1 # C'est l'indice du END

      return index_next_word, matrix[index_previous_ngram][index_next_word]  # retourne l'index du prochain mot, et le nombre d'occurence de ce prochain mot sachant le mot precedent dans le corpus


    #fonction permettant de retrouver le ngram de debut de phrase a partir d'un ngram quelconque, elle prend en entree l'index d'un ngram et retourne un index d'un ngram
    def get_begining_sentence(self, index_ngram, matrix):
        print(" get beginning sentence from a part of sentence ...")
        # 1. get the word 10 gram
        ngram_start = self.index_to_word_ngram[index_ngram] # cest une liste des tokens du ngram
        if "START" in ngram_start : # il s'agit d'un n gram de debut de phrase cest a dire qu'il contient les START, je retourne le meme index
          return [index_ngram]

        else: # ici je recupere tous les n grams tel que le successeur est le premier mot de la sequence

          list_index_all_ngram_available = []
          ngram_start = ngram_start.split(' ') # je convertis la chaine ngram en liste pour pouvoir la manipuler aisement
          print(ngram_start, "le ngram de depart")
          for index_ngram_1 in list(self.word_to_index_ngram.values()) : # je parcours chaque ngram, s'il a pour successeur, le premier mot du ngram de depart, je le recupere, j'utilise les ids
            first_word_ngram = ngram_start[0] # je recupere le premier mot du ngram de depart
            index_first_word_ngram = self.word_to_index_unigram[first_word_ngram] # je recupere l'index du premier mot du ngram de depart
            index_successor, occurence = self.get_next_word(index_ngram_1, self.matrix) # Je recupere le successeur de la sequence trouvee actuelle
            if index_successor == index_first_word_ngram : # je verifie si le premier mot du ngram de depart est bel et bien le successeur du ngram trouvé
              list_index_all_ngram_available.append(index_ngram_1) # si oui je garde le ngram en question
              #print(">>", index_ngram_1, self.index_to_word_ngram[index_ngram_1])
          
          index_return = [] # index des ngram de retour

          for index_ngram_2 in list_index_all_ngram_available :# pour chaque ngram retenu, je verifie maintenant s'il est le ngram correspondant en testant si les successeur formés de chaque sous ngram formé avec le ngram de depart correspondent 
            ngram_instance = self.index_to_word_ngram[index_ngram_2].split(' ') # un des ngrams retenus (qui soit le precedent du premier mot du ngram de depart), cest une liste
            for i in range(1, self.ngram_size): # Cette boucle me permet de tester tous les sous ngrams et selectionner le vrai et bon ngram
              ngram_to_test = ngram_instance[i:] + ngram_start[:i] # je constitue le ngram que je vais tester pour verifier son successeur
              ngram_to_test = " ".join(ngram_to_test) # pour pouvoir recuperer ca dans le dictionnaire
              #print(ngram_to_test, "celui qu'on a formé")
              #print(ngram_start[i+1], "qui est cense etre le successeur")
              index_ngram_to_test = self.word_to_index_ngram[ngram_to_test] # je recupere son indice dans le dictionnaire mot index, mot ici qui est une liste
              index_successor, occurence = self.get_next_word(index_ngram_to_test, self.matrix) # Je recupere le successeur de ce ngram_to_test (son vrai successeur)
              index_successor_to_test = self.word_to_index_unigram[ngram_start[i]] # je verifie si les index correspondent 
              if index_successor != index_successor_to_test : # si non je passe sur cet ngram
                break # je sors de cette boucle
              else:
                print("egal")

              index_return.append(index_ngram_2)
          
        return index_return
          

    # Entrainement du modele (creer la liste des sequences, le dictionnaire des sequences, la matrice sequence successeur)
    def train(self, corpus, ngram_size = 10):

      self.ngram_size = ngram_size # de ce fait, la taille des grams sera directement changee par celle qui est passee en parametre

      print(" Model training ...")

      #Get word to index and index to word for unigrams
      self.word_to_index_unigram, self.index_to_word_unigram = self.create_word_index_unigram(corpus) # je contruis le dictionnaire et la liste des tokens du corpus en fonction de la taille de ngram (taille de la demie phrase maxi 10)

      #Get word to index and index to word for ngrams
      self.word_to_index_ngram, self.index_to_word_ngram = self.create_word_index_ngram(corpus, ngram_size) # Idem matrice equivalente

      #Get ngram matrix
      self.matrix = self.create_ngram_matrix(self.word_to_index_ngram, self.index_to_word_ngram, self.word_to_index_unigram, corpus, ngram_size)

      print(" Model training Success !!! ")

    # Fonction pour generer les phrases
    def generate_sentence(self, previous_text, nb_words_after=100): # Il prend juste la phrase à completer, et un parametre qui permet de specifier le nombre de mots qu'il veut à la suite

      print(" Text generation ... \n") # Design

      #1. tokenisation de la phrase en entree
      used_words = self.return_token(previous_text)
      while ' ' in used_words :
          used_words.remove(' ') # J'enleve dans les espaces dans la phrases à completer

      #2. Recuperons les n derniers mot de la demie phrase si n<10 et 10 sinon
      
      last_ngram = "" # J'initialise la variable qui va contenir la demie phrase (ou alors la sequence de 10 mots de la demir phrase)

      if len(used_words) == 0: # S'il n'entre rien genre une chaine vide, je lui retourne ce message d'erreur
          return "Enter a text with at least one character..."

      elif len(used_words) < self.ngram_size: # Cas ou la demie phrase a moins de 10 caracteres, je vais concatener le ngram avec les START devant

          chaine_start = "START " * (self.ngram_size-len(used_words)) # je constitue le debut de la chaine si jamais la phrase a moins de 10 mots

          for h in range(len(used_words), 0, -1):
            last_ngram = last_ngram + used_words[-h] + " " #  Idem je constitue la sequence ...
          
          last_ngram = chaine_start + last_ngram # je concatene les start avec la phrase entree

      else: 

         for h in range(self.ngram_size, 0, -1):
            last_ngram = last_ngram + used_words[-h] + " " #  Idem je constitue la sequence ...   
      
      last_ngram = last_ngram[:-1] # Pour enlever l'espace de la fin

      result = previous_text + " " # J'initialise la variable qui va contenir la phrase complete a la fin

      list_tuple_last_ngram_used_words_result = []
      try:
          index_last_ngram = self.word_to_index_ngram[last_ngram] # Je recupere donc l'index de ce ngram
          list_tuple_last_ngram_used_words_result.append((last_ngram, used_words, result))
      except KeyError: # cas ou la sequence de mot ne corresponds pas à un début de phrase, je cherche toutes les combinaisons avec ces mots entrés
          all_combinations = itertools.permutations(used_words, len(used_words)) # je fabirque les combinaisons
          for one_ngram in all_combinations:#je parcours chacune de ces combinaisons
              last_ngram = ""
              for i in one_ngram:
                  last_ngram = last_ngram + i + " " # je constitue la sequence avec les mots de la phrase
              last_ngram = last_ngram[:-1] # j'enleve l'espace de la fin
              if len(used_words) < self.ngram_size: # dans le cas ou le nombre de mots etait moins de 10, je complete
                  chaine_start = "START " * (self.ngram_size-len(used_words)) # je constitue le debut de la chaine si jamais la phrase a moins de 10 mots
                  chaine_start = chaine_start + last_ngram # je concatene les start avec la phrase entree
              else:
                  chaine_start = last_ngram # je garde la valeur du last_ngram que j'avais d'abord
              if chaine_start in list(self.word_to_index_ngram.keys()):# Si la chaine fait partie d'une sequence dans la matrice constituee, je la conserver
                  used_words = last_ngram.split(" ")
                  result = last_ngram + " "
                  list_tuple_last_ngram_used_words_result.append((chaine_start, used_words, result))
              else: # pour dans ce cas verifier si le last_ngram est une element d'un 10gram pas forcement d'un 10gram qui est un debut de phrase
                  for line in list(self.word_to_index_ngram.keys()):
                      if last_ngram in line:
                          used_words = line.split(" ")
                          used_words = [value for value in used_words if value != 'START' and value !=' ']
                          result = ' '.join(used_words) + " "
                          list_tuple_last_ngram_used_words_result.append((line, used_words, result))
                      
      list_output = [] # ici donc le resultat sera une liste, de chaque different resultat obtenu, que ce soit pour chaque ngram ou pour chaque ensemble de resultat generee par chque last_ngram

      for last_ngram, used_words, result in list_tuple_last_ngram_used_words_result[:10]:
          
          result_copy = result
          used_words_copy = used_words[:]          
      
          index_last_ngram = self.word_to_index_ngram[last_ngram] # Je recupere donc l'index de ce ngram

          #print(self.get_begining_sentence(index_last_ngram, self.matrix))

          index_last_ngram_copy = index_last_ngram # je fais une copie de l'index de la demie phrase pour recalculer la suite du texte autant de fois plus bas

          if len(used_words) >= 10 : # Si la demie phrase a plus de 06 mots, je lui retourne un seul resultat

              for i in range(nb_words_after):  # Je boucle au plus le nombre de fois qu'il veut de mots 
                  
                  index_next_word, occurence = self.get_next_word(index_last_ngram, self.matrix) # Je recupere le mot suivant de la derniere sequence

                  if(index_next_word!=1):        # Je verifie que le mot que je veux ajouter n'est pas le mot END car cest la fin de la phrase, si cest le cas, je ne l'ajoute juste pas
                    used_words.append(self.index_to_word_unigram[index_next_word]) 
                    result = result + self.index_to_word_unigram[index_next_word] + " "
                  else:
                      break;    

                  last_ngram = ""
                  for h in range(self.ngram_size, 0, -1): # Pour reconstituer la liste de ngram apres avoir trouvé le successeur du précédent
                    last_ngram = last_ngram + used_words[-h] + " "    
                  index_last_ngram = self.word_to_index_ngram[last_ngram[:-1]]

              list_output.append(result[:-1]) # Je retourne donc son un resultat
          
          else : # je veux generer plusieurs sorties si le nombre de mots est moins de 10 caracteres
              nombre_de_sortie = random.randint(6,10) # cette variable contient un nombre aleatoire de resultat que je retourne dans ce cas

              for _ in range(nombre_de_sortie): # Je boucle donc pour faire plusieurs predictions, pour generer plusieurs resultats comme à la base, le nombre de mot etait petit
                  index_last_ngram = index_last_ngram_copy # Je reinitialise donc l'index a la sequence de debut pour refaire la meme operation
                  result = result_copy # Je reinitialise la variable resultat
                  used_words = used_words_copy[:] # Egalement la liste des tokens de la demie phrase
                  while ' ' in used_words : # J'enleve encore tous les espaces dedans
                      used_words.remove(' ')
                  taille_last_ngram = len(used_words[:])

                  for i in range(nb_words_after):  # Je boucle au plus le nombre de fois qu'il veut de mots 
                  
                      index_next_word, occurence = self.get_next_word(index_last_ngram, self.matrix) # Je recupere le mot suivant de la derniere sequence

                      if(index_next_word!=1):        # Je verifie que le mot que je veux ajouter n'est pas le mot END car cest la fin de la phrase, si cest le cas, je ne l'ajoute juste pas
                        used_words.append(self.index_to_word_unigram[index_next_word])
                        result = result + self.index_to_word_unigram[index_next_word] + " "
                      else:
                          break;    

                      # Je vais modifier la taille des ngrams et reformuler la matrice afin de considerer pour la prochaine etape plus une sequence de mot plus grand (chose qui ameliore le resultat)
                      if taille_last_ngram < self.ngram_size : # je m'arrete a 10 car, 10 caracteres precedeents pour trouver le suivant est, on va dire  suffisant!
                          
                          taille_last_ngram = taille_last_ngram + 1 # j'incremente la taille de la sequene de mot que je prends pour la prochaine etape
                          """ mettre a jour la valeur de last_ngram avec les START START, etc """  

                      last_ngram = ""
                      chaine_start = "START " * (self.ngram_size-taille_last_ngram) # je constitue le debut de la chaine si jamais la phrase a moins de 10 mots                  
                      for h in range(taille_last_ngram, 0, -1):
                        last_ngram = last_ngram + used_words[-h] + " " #  Idem je constitue la sequence ...
                      
                      last_ngram = chaine_start + last_ngram # je concatene les start avec la les derniers mots de la phrase

                      index_last_ngram = self.word_to_index_ngram[last_ngram[:-1]]
                  
                  list_output.append(result[:-1]) # J'ajoute un  des resultats sorti

      # Si la liste est vide, alors je retourne un message d'erreur
      if len(list_output) == 0:
        return "Sorry, I don't understand what you mean.."
      
      list_output = list(set(list_output)) # je supprime les doubons dans la liste des phrases à retourner
      list_output_returned = []
      for sentence in list_output: # Pour un meilleur formatage
        list_output_returned.append(sentence)
      return list_output_returned # je retourne la liste

    def evaluate(self, references, candidat, weights = (1/4,1/4,1/4,1/4)):
      return sentence_bleu(references, candidat, weights)
      

