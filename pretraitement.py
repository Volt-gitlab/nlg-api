
import spacy # importation de la librairie
import csv # importation de la librairie permettant de creer le fichier csv


#Me permettait de mettre les donnees deja traitees dans le csv, je ne l'e'xecute que si je modifie mes donnees ou si mon csv est vide

# Fonction de tokenisation
def return_token(sentence):

  nlp = spacy.load("fr_core_news_sm")
  # Tokeniser la phrase
  doc = nlp(sentence)
  # Retourner le texte de chaque token
  return [X.text for X in doc]


# Extraction du corpus avec ponctuation
liste = []
with open("corpus.txt", "r", encoding='latin-1') as f:
  for line in f.readlines():
    liste.append(line)


# Tokenisation du corpus
corpus_with_punc = [] # corpus est la liste des tokens
for line in liste:
  print(liste.index(line))
  result = return_token(line)[:-1]
  corpus_with_punc.append(result)

print("-----4")
# Enlever la ponctuation dans le corpus
punc = [':', "!", "^", '.', '`', '~', ';', ',', '?', '...', '_', ' ']
corpus = []
for sentence in corpus_with_punc:
  liste_tokens = []
  for token in sentence:
    if token not in punc :
      liste_tokens.append(token)
  corpus.append(liste_tokens)


# i save that informations in the csv file
# i create the file
with open('references.csv', 'w', encoding='UTF8') as f :
    writer = csv.writer(f)

    # i insert data
    for reference in corpus:    
      print(corpus.index(reference), " csv")
      suite_token = ""
      for token in reference:
        suite_token = suite_token + token + ' '
      print(suite_token, len(suite_token))
      writer.writerow(suite_token)
